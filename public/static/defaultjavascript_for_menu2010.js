$(document).ready(function() {
  $('#nav-prodnav').sooperfish({ dualColumn: 99,
                                 tripleColumn: 99,
                                 speedShow: 250,
                                 autoArrows: true
                               });

  /* Depending on the localization, the top most menu items may contain
     wrapped labels (stupid verbose languages). To prevent a 'jagged' menu,
     respectively the need for locale dependent CSS, we do some adjustments
     here. First we calculate the height of the highest menu item, then we set
     the height of all menu items to this value and finally we add this value
     the overall height of the menu bar so everything will line up nicely
     (hopefully). */
  var menuItems = $('#nav-prodnav > li > a');
  maxHeight = 0;
  menuItems.each(function() {
    if ($(this).height() > maxHeight) {
      maxHeight = $(this).height();
    }
  });

  menuItems.css('height', maxHeight);

  var menubar = $('#menubar');
  menubar.css('height', menubar.height() + maxHeight);

  /* Disable click through for parents, even when baseref is used
     and '#' will point go back to the home page */
  $('.sf-parent > a').click(function() { return false });
});
