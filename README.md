# pro-cold.eu Static Page

![Build Status](https://gitlab.com/topten-dev/pro-cold-static/badges/master/build.svg)

---

This is a static page representation of the old pro-cold.eu website with a prominent announcement about the status of the website.

This project uses Gitlab Pages to automatically publish all changes made to the files in the `public` folder, into the pro-cold.eu website.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Urls](#urls)
- [Networking](#networking)
- [Editing](#editing)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine:latest

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - master
```

The above configuration expects to put all your HTML files in the `public/` directory.

## Urls

This project is deployed automatically in the following urls:

1. [pro-cold.eu](https://pro-cold.eu)
2. [topten-dev.gitlab.io/pro-cold-static/](https://topten-dev.gitlab.io/pro-cold-static/)

## Networking

The networking stack is the following currently:

1. Gitlab CI/CD
2. Gitlab Pages deploy
3. Cloudflare

## Editing

You can manually edit the files under the `public/` folder in this repository and after pushing the changes, it will automatically deploy the changes into the website(s).
